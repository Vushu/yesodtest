{-# LANGUAGE PackageImports #-}
import "CardCollector" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
