{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeFamilies      #-}
module Handler.GetCard where
--import Data.Text (Text)
import Import

data UnitAbility = UnitAbility {
    abilityType :: Maybe Text, 
    zone :: Maybe Text,
    limitBreak :: Maybe Text,
    generationBreak :: Maybe Text,
    text :: Maybe Text
}

--zone vanguard, rareguardzone 

    --generation breake og limitbreak

data Card = Card {
    name :: Text,
    grade :: Int,
    cardType :: Text, -- normal trígger
    skillEffect :: Text,
    triggerIcon :: Maybe Text,
    race :: Maybe Text,
    country :: Maybe Text,
    clan :: Text,
    critical :: Int,
    attack :: Int,
    defense :: Int,
    quote :: Maybe Text,
    description :: Maybe [UnitAbility],
    imageLink :: Maybe Text,
    flagImageLink :: Maybe Text,
    illustrator :: Maybe Text,
    rarity :: Maybe Text,
    serie :: Maybe Text
    }

instance ToJSON UnitAbility where
    toJSON UnitAbility {..} = object 
        [ "abilityType" .= abilityType
        , "zone" .= zone
        , "limitBreak" .= limitBreak
        , "generationBreak" .= generationBreak
        , "text" .= text
        ]

instance ToJSON Card where
    toJSON Card {..} = object
        [ "name" .= name
        , "grade" .= grade
        , "cardType" .= cardType
        , "skillEffect" .= skillEffect
        , "triggerIcon" .= triggerIcon
        , "race" .= race
        , "country" .= country
        , "clan" .= clan
        , "critical" .= critical
        , "attack" .= attack
        , "defense" .= defense
        , "quote" .= quote
        , "description" .= description
        , "imageLink" .= imageLink
        , "flagImageLink" .= flagImageLink
        , "illustrator" .= illustrator
        , "rarity" .= rarity
        , "serie" .= serie
        ]

getGetCardR :: Int -> Handler Value
getGetCardR int = returnJson $ testCard

testCard :: Card
testCard = 
        Card 
        "Flower Garden Maiden, Mylis" 0 "Trigger" "Boost" (Just "critical") (Just "Bioroid") Nothing 
        "Neo necta" 1 4000 10000 (Just "Take this!") 
        (Just [(UnitAbility (Just "Automatic") (Just "Vanguard") Nothing Nothing (Just "Likes big Ass"))]) 
        Nothing Nothing Nothing Nothing Nothing 



